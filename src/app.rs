mod components;

use leptos::*;
use leptos_meta::*;
use leptos_router::*;
use components::sidebar::*;
use components::navbar::*;


#[component]
pub fn App(cx: Scope) -> impl IntoView {

    let navbar_classes = "top-0 left-0 md:left-64".to_string();
    let (dark_mode, dark_mode_writer) = create_signal(cx, false);
    use gloo_storage::Storage;
    create_effect(cx, move |_| {
        let storage = gloo_storage::LocalStorage::raw();
        if storage.get("theme").unwrap_or(Some(String::from("light"))) == Some(String::from("dark")) {
            dark_mode_writer.set(true);
        }
    });
    create_effect(cx, move |_| {
        let storage = gloo_storage::LocalStorage::raw();
        _ = storage.set("theme", if dark_mode() {"dark"} else {"light"});
    });

    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context(cx);

    view! {
        cx,
        <Html attributes=AdditionalAttributes::from(vec![("class", move || if dark_mode() {"dark bg-gradient-to-r from-blue-900 to-cyan-900".to_string()} else {"bg-gradient-to-r from-blue-200 to-cyan-200".to_string()})])/>
        // injects a stylesheet into the document <head>
        // id=leptos means cargo-leptos will hot-reload this stylesheet
        <Stylesheet id="leptos" href="/pkg/ncp_web.css"/>
        <Stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.6/flowbite.min.css"/>
        <Script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.6/flowbite.min.js"/>

        // content for this welcome page
        <NavBar class=navbar_classes/>
        <SideBar set_dark_mode=dark_mode_writer get_dark_mode=dark_mode/>
        <div class="w-full h-20"/>
        <Router>
            <main class="text-gray-900 dark:text-white">
                <Routes>
                    <Route path="" view=|cx| view! { cx, <HomePage/> }/>
                </Routes>
            </main>
        </Router>
    }
}

/// Renders the home page of your application.
#[component]
fn HomePage(cx: Scope) -> impl IntoView {
    // Creates a reactive value to update the button
    let (count, set_count) = create_signal(cx, 0);
    let on_click = move |_| set_count.update(|count| *count += 1);
    let sidebar_classes = "left-0 top-0".to_string();

    view! { cx,
        <h1>"Welcome to Leptos!"</h1>
        <button on:click=on_click>"Click Me: " {count}</button>
    }
}
