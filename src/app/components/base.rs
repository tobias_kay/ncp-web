use leptos::*;

#[component]
pub fn Test(cx: Scope) -> impl IntoView {
    view! {cx,
        <div>"TEST"</div>
    }
}
